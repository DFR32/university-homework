#include "asd_linked_list.hpp"

// better than the macro hell from C
template <typename T>
void SafeCleanNode(T& ptr)
{
    // Turn it into a dummy node, we cannot deallocate
    // ... as it would deallocate the entire memory block
    // ... we'll let users deal with the library as they wish

    ptr->next = nullptr;
}

template <typename T>
void SafeDeleteListRef(T& ptr)
{
    delete[] ptr;
    ptr = nullptr;
}

int main()
{
    // Declare a new linked list
    LinkedList<uint64_t> NewList;

    // Dynamically allocate the memory region
    // ... for the nodes
    LLNode<uint64_t>* node_memory_area = new LLNode<uint64_t>[10];

    // Iterate the allocated memory and fill it properly
    for(size_t i = 0; i < 9; ++i)
    {
        node_memory_area[i].value = (17 * i) % 256;
        NewList.AddNodeToStart(&node_memory_area[i]);
    }

    // Test more features...
    node_memory_area[9].value = (17 * 9) % 256;
    NewList.AddNodeToEnd(&node_memory_area[9]);

    // Display the list in this state
    NewList.Display(std::cout);

    // Padding to see the changes better
    std::cout << "\n\n\n\n";

    // Remove a node with value 0, we know we have one
    LLNode<uint64_t>* fetch_node = NewList.RemoveNodeWithValue(0);

    // We have retrieved the bit of memory, show it!
    std::cout << "We have popped node " << std::hex << fetch_node << " from the memory pool and we are preparing a deallocation.\n\n\n";

    SafeCleanNode(fetch_node);

    // Display the list again
    NewList.Display(std::cout);

    // Padding to see the changes better
    std::cout << "\n\n\n\n";

    // Remove a node with value 0, we know we have one
    fetch_node = NewList.RemoveNodeAtPosition(2);

    // We have retrieved the bit of memory, show it!
    std::cout << "We have popped node " << std::hex << fetch_node << " from the memory pool and we are preparing a deallocation.\n\n\n";

    SafeCleanNode(fetch_node);

    // Display the list again
    NewList.Display(std::cout);

    // Padding to see the changes better
    std::cout << "\n\n\n\n";

    LLNode<uint64_t>* new_node = new LLNode<uint64_t>();
    new_node->value = 102305;

    // We have retrieved the bit of memory, show it!
    std::cout << "We are adding an intermediary node by the address " << std::hex << new_node << " to the linked list. It is going to be chained after node number 3.\n\n\n";

    NewList.AddIntermediaryNode(new_node, 3);

    // Display the list again
    NewList.Display(std::cout);

    // Padding to see the changes better
    std::cout << "\n\n\n\n";

    NewList.Apply([](uint64_t& i) { return i + 6; });

    std::cout << "We set all elements to val + 6.\n\n\n";

    // Display the list again
    NewList.Display(std::cout);

    std::cout << "\n\n\n";

    // Print the clearance message
    std::cout << "We cleared the linked list.\n\n\n";

    NewList.Clear();
    SafeDeleteListRef(node_memory_area);

    // Display the list again
    NewList.Display(std::cout);
    return 0;
}

#include "asd_linked_list.inl"
