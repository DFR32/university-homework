//
// Created by Shad on 11/21/2018.
//
#ifndef ASD_5_QUEUE_H
#define ASD_5_QUEUE_H

#include <iostream>

#define MEMORY_PAGE_SIZE 1024

template <typename T>
class Queue
{
private:
    T* _local_memory;
    size_t _array_elements;
    size_t allocated_memory;

public:
    Queue() noexcept
            :
            _local_memory(nullptr),
            _array_elements(0),
            allocated_memory(0) {}

    explicit Queue(size_t elements)
    {
        allocated_memory = (((elements * sizeof(T)) / MEMORY_PAGE_SIZE) + 1) * MEMORY_PAGE_SIZE;

        _local_memory = reinterpret_cast<T*>(malloc(allocated_memory));

        if(_local_memory == nullptr)
            throw std::runtime_error("Error! Constructor cannot allocate memory, returned null pointer!");

        _array_elements = 0;
    }

    Queue(size_t elements, const T& initializer)
    {
        allocated_memory = (((elements * sizeof(T)) / MEMORY_PAGE_SIZE) + 1) * MEMORY_PAGE_SIZE;

        _local_memory = reinterpret_cast<T*>(malloc(allocated_memory));

        if(_local_memory == nullptr)
            throw std::runtime_error("Error! Constructor cannot allocate memory, returned null pointer!");

        for(size_t i = 0; i < elements; ++i)
            _local_memory[i] = initializer;

        _array_elements = 0;
    }

    explicit Queue(const T& other)
    {
        allocated_memory = other.allocated_memory;

        _local_memory = reinterpret_cast<T*>(malloc(other.allocated_memory));

        if(_local_memory == nullptr)
            throw std::runtime_error("Error! Constructor cannot allocate memory, returned null pointer!");

        for(size_t i = 0; i < other._array_elements; ++i)
            _local_memory[i] = other._local_memory[i];

        _array_elements = other._array_elements;
    }

    void push(const T& element)
    {
        if( ((int32_t)allocated_memory - (int32_t)((_array_elements + 1) * sizeof(T))) < 0 )
            throw std::runtime_error("Error! Ran out of memory!");

        if(_array_elements == 0)
        {
            _local_memory[_array_elements ++] = element;
            return;
        }

        for(int64_t i = (int64_t)_array_elements - 1; i >= 0; --i)
            _local_memory[i + 1] = _local_memory[i];

        _local_memory[0] = element;
        ++ _array_elements;
    }

    T pop()
    {
        std::cout << "Array elements: " << _array_elements << '\n';

        if(_array_elements == 0)
            throw std::runtime_error("Error! Cannot pop from an empty queue.");

        return _local_memory[-- _array_elements];
    }

    T peek()
    {
        if(_array_elements == 0)
            throw std::runtime_error("Error! Cannot peek in an empty queue.");

        return _local_memory[_array_elements - 1];
    }

    bool empty()
    {
        return _array_elements == 0;
    }

    int64_t search(const T& element)
    {
        int64_t distance_from_top = 0;

        if(_array_elements == 0)
            return -1;

        for(int64_t i = 0; i < _array_elements; ++i)
        {
            if(element == _local_memory[i])
                return distance_from_top;
            else ++ distance_from_top;
        }

        return -1;
    }

    void display(std::ostream& output)
    {
        if(_array_elements == 0)
            return;

        for(int64_t i = 0; i < (int64_t)_array_elements; ++i)
            output << _local_memory[i] << '\t';
        output << '\n';
    }
};

#endif //ASD_5_QUEUE_H
