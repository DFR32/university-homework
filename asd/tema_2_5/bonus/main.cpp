#include <stdio.h>
#include <stdint.h>
#include <string.h>

int main()
{
    char input_line[100];
    char margin[100] = {};

    uint32_t margin_size = 0;
    uint32_t string_size = 0;
    uint32_t i = 0, j = 0;

    scanf("%99s", input_line);
    string_size = (uint32_t)strlen(input_line);

    for(; i < string_size; ++i)
    {
        if(margin_size == (string_size - 1))
            break;

        j = string_size - i - 1;

        if((i != j) && (input_line[i] == input_line[j]))
        {
            margin[margin_size++] = input_line[i];
        }
        else break;
    }

    printf("%s\n%d\n", margin, (int32_t)margin_size);

    return 0;
}
