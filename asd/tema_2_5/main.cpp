#include "stack.h"
#include "queue.h"

#include <iostream>
#include <cstdint>
#include <cstdio>

int main()
{
    // Constructor test
    Stack<uint32_t> a(3, 0);

    // Push test
    a.push(2);
    a.push(1);
    a.push(3);

    // Pop test
    printf("Pop: %d\n", a.pop());
    printf("Pop: %d\n", a.pop());
    printf("Pop: %d\n", a.pop());

    // Peek test
    a.push(4);
    printf("Peek: %d\n", a.peek());

    // Is empty?
    printf("Is empty?: %d\n", a.empty() ? 1 : 0);

    // Search test
    a.push(7);
    a.push(2);
    a.push(6);
    a.push(8);
    a.push(9);

    printf("Search for 6: %lld\n", a.search(6));
    printf("Search for 17: %lld\n", a.search(17));

    // Display stack
    a.display(std::cout);


    // QUEUEs

    Queue<uint32_t> b(10, 0);

    b.push(6);
    b.push(16);
    b.push(4);

    // Pop test
    printf("Pop: %d\n", b.pop());

    // Peek test
    b.push(3);
    printf("Peek: %d\n", b.peek());

    // Is empty?
    printf("Is empty?: %d\n", b.empty());

    // Search
    printf("Position of 16: %lld\n", b.search(16));

    // Display
    b.display(std::cout);
    return 0;
}