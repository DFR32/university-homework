#include "asd_linked_list.inl"

#include <cstdio>
#include <iostream>
using namespace std;

template <typename T>
inline LLNode<T>* clone_node(LLNode<T> a)
{
    LLNode<T>* b = new LLNode<T>;
    *b = a;
    return b;
}

template <typename T>
LinkedList<T>* reverse_linked_list_new(LinkedList<T>* given_list)
{
    size_t list_size = 0;

    LinkedList<T>* new_list = new LinkedList<T>(nullptr);

    for(LLNode<T>* start = given_list->GetStart(); start != nullptr; start = start->next)
        ++ list_size;


    for(LLNode<T>* tmp_node = given_list->GetStart(); tmp_node != nullptr; tmp_node = tmp_node->next)
    {
        new_list->AddNodeToStart(clone_node(*tmp_node));

        -- list_size;
        if(list_size == 0)
            break;
    }

    new_list->Display(std::cout);
    return new_list;
}

template <typename T>
LinkedList<T>* reverse_linked_list(LinkedList<T>* given_list)
{
    LLNode<T>* last_node = nullptr, *next_ptr = nullptr, *iter = given_list->GetStart();

    while(iter != nullptr)
    {
        next_ptr = iter->next;
        iter->next = last_node;

        last_node = iter;
        iter = next_ptr;
    }
    given_list->SetStart(last_node);
    return given_list;
}

template <typename T>
LinkedList<T>* combine_lists(LinkedList<T>* list_1, LinkedList<T>* list_2)
{
    LinkedList<T>* result_list = new LinkedList<T>(nullptr);

    bool filled = true;
    LLNode<T>* last_item = nullptr;

    for(LLNode<T>* first_iter = list_1->GetStart(); first_iter != nullptr;)
    {
        if(!filled)
            break;

        for(LLNode<T>* second_iter = list_2->GetStart(); second_iter != nullptr;)
        {
            if( first_iter->next == nullptr )
            {
                filled = false;
                last_item = second_iter;
                break;
            }

            if( second_iter->next == nullptr )
            {
                filled = false;
                last_item = first_iter;
                break;
            }

            if((first_iter->value) < (second_iter->value))
            {
                result_list->AddNodeToStart(clone_node(*first_iter));
                first_iter = first_iter->next;
                continue;
            }
            else
            {
                result_list->AddNodeToStart(clone_node(*second_iter));
                second_iter = second_iter->next;
                continue;
            }
        }
    }

    if(filled)
    {
        for(LLNode<T>* iter = last_item; iter != nullptr; iter = iter->next)
            if(result_list->GetNodeWithValue(iter->value) == nullptr)
                result_list->AddNodeToStart(clone_node(*iter));
    }

    return result_list;
}

template <typename T>
void distribute(LinkedList<T>* given_list)
{
    LinkedList<T>* odd_list = new LinkedList<T>(nullptr);
    LinkedList<T>* even_list = new LinkedList<T>(nullptr);

    uint32_t position = 0;

    for(LLNode<T>* iter = given_list->GetStart(); iter != nullptr; iter = iter->next)
    {
        if(position & 1)
            odd_list->AddNodeToStart(clone_node(*iter));
        else
            even_list->AddNodeToStart(clone_node(*iter));

        ++ position;
    }

    odd_list->Display(std::cout);
    even_list->Display(std::cout);
}

template <typename T>
struct matrix_node
{
    size_t column_id;
    T value;

    void operator=(matrix_node& other)
    {
        this->column_id = other.column_id;
        this->value     = other.value;
    }
};

template <typename T>
T dereference_node(T* matrix, T row, T column, T columns)
{
    return *((matrix + row * columns) + column);
}

template <typename T>
using ListOfLists = LinkedList<LinkedList<matrix_node<T>*>*>*;

template <typename T>
ListOfLists<T> get_matrix_as_lists(T* matrix, size_t rows, size_t columns)
{
    ListOfLists<T> result_list = new LinkedList< LinkedList< matrix_node<T>* >* >(nullptr);
    LinkedList<matrix_node<T>* >* tmp_list = nullptr;
    matrix_node<T>* tmp_node = nullptr;

    LLNode<matrix_node<T>*>* tmp_list_node = nullptr;
    LLNode<LinkedList<matrix_node<T>*>*>* tmp_llist_node = nullptr;

    for(size_t i = 0; i < rows; ++i)
    {
        tmp_list = new LinkedList< matrix_node<T>* >(nullptr);
        tmp_llist_node = new LLNode<LinkedList<matrix_node<T>*>*>();

        for(size_t j = 0; j < columns; ++j)
        {
            if(dereference_node<T>(matrix, i, j, columns) == 0)
                continue;

            tmp_list_node = new LLNode<matrix_node<T>*>();

            tmp_node = new matrix_node<T>();
            tmp_node->column_id = j;
            tmp_node->value = dereference_node<T>(matrix, i, j, columns);

            tmp_list_node->value = tmp_node;
            tmp_list->AddNodeToStart(tmp_list_node);
        }

        tmp_llist_node->value = tmp_list;

        result_list->AddNodeToStart(tmp_llist_node);
    }

    return result_list;
}

template <typename T>
LinkedList<LinkedList<matrix_node<T>*>*>* add_lists_of_lists(ListOfLists<T> list_1, ListOfLists<T> list_2)
{
    LinkedList<LinkedList<matrix_node<T>*>*>* new_list = new LinkedList<LinkedList<matrix_node<T>*>*>(nullptr);

    size_t rows = 0, columns = 0;

    for(LLNode<LinkedList<matrix_node<T>*>*>* iter_1 = list_1->GetStart(); iter_1 != nullptr; iter_1 = iter_1->next)
    {
        LinkedList<matrix_node<T>*>* tmp_list = new LinkedList<matrix_node<T>*>(nullptr);
        LLNode<LinkedList<matrix_node<T>*>*>* tmp_node = new LLNode<LinkedList<matrix_node<T>*>*>();

        columns = 0;
        for(LLNode<matrix_node<T>*>* iter_node_1 = iter_1->value->GetStart(); iter_node_1 != nullptr; iter_node_1 = iter_node_1->next)
        {
            tmp_list->AddNodeToStart(clone_node(*iter_node_1));

            ++ columns;
        }

        tmp_node->value = tmp_list;
        new_list->AddNodeToStart(tmp_node);

        ++ rows;
    }

    size_t position_list = 0, position_node_list = 0;

    for(LLNode<LinkedList<matrix_node<T>*>*>* iter_1 = list_1->GetStart(), *iter_2 = list_2->GetStart(); iter_2 != nullptr; iter_1 = iter_1->next)
    {
        position_node_list = 0;

        for(LLNode<matrix_node<T>*>* node_iter_2 = iter_2->value->GetStart(), *node_iter_1 = iter_1->value->GetStart(); node_iter_2 != nullptr; node_iter_2 = node_iter_2->next)
        {
            if(node_iter_1 == nullptr)
            {
                new_list->GetNodeAtPosition(position_list)->value->AddNodeToStart(clone_node(*node_iter_2));
                continue;
            }

            if(node_iter_1->value->column_id == node_iter_2->value->column_id)
            {
                new_list->GetNodeAtPosition(rows - position_list - 1)->value->GetNodeAtPosition(columns - position_node_list - 1)->value->value += node_iter_2->value->value;
            }

            ++ position_node_list;
            node_iter_1 = node_iter_1->next;
        }

        ++ position_list;
        iter_2 = iter_2->next;
    }

    return new_list;
}

int main()
{
    LLNode<unsigned>* new_nodes     = new LLNode<unsigned int>[32];
    LLNode<unsigned>* other_nodes   = new LLNode<unsigned int>[32];

    LinkedList<unsigned>* start_list = new LinkedList<unsigned int>(nullptr);
    LinkedList<unsigned>* other_list = new LinkedList<unsigned int>(nullptr);

    for(unsigned i = 0; i < 32; ++i)
    {
        new_nodes[i].value = i * 3;

        start_list->AddNodeToStart(&new_nodes[i]);
    }

    for(unsigned i = 0; i < 32; ++i)
    {
        other_nodes[i].value = i * 5;

        other_list->AddNodeToStart(&other_nodes[i]);
    }

    start_list->Display(std::cout);
    printf("\n\n\n");
    other_list->Display(std::cout);
    printf("\n\n\n");

    // --------- TEST 1 ---------
    /*
    start_list->Display(std::cout);
    std::cout << "\n\n\n\n\n";
    reverse_linked_list_new(start_list);
    */

    // --------- TEST 2 ---------
    /*
    given_list->Display(std::cout);
    reverse_linked_list(start_list);
    given_list->Display(std::cout);
    */

    // --------- TEST 3 ---------

    /*
    LinkedList<unsigned int>* result_list = combine_lists(start_list, other_list);
    result_list->Display(std::cout);
    */

    // --------- TEST 4 ---------
    //distribute(start_list);

    // --------- TEST 5 ---------
    unsigned demo_matrix[4][3] =
    {
        { 1, 2, 3 },
        { 4, 5, 6 },
        { 7, 8, 9 },
        { 10, 11, 12}
    };

    unsigned demo_matrix_2[4][3] =
    {
        { 3, 2, 1 },
        { 6, 5, 4 },
        { 9, 8, 7 },
        { 12, 11, 10 }
    };

    LinkedList<LinkedList<matrix_node<unsigned>*>*>* result_list = get_matrix_as_lists((unsigned*)demo_matrix, 4, 3);
    LinkedList<LinkedList<matrix_node<unsigned>*>*>* new_result  = get_matrix_as_lists((unsigned*)demo_matrix_2, 4, 3);

    LinkedList<LinkedList<matrix_node<unsigned>*>*>* new_list = add_lists_of_lists(result_list, new_result);

    for(LLNode<LinkedList<matrix_node<unsigned>*>*>* iter = new_list->GetStart(); iter != nullptr; iter = iter->next)
    {
        for(LLNode<matrix_node<unsigned>*>* node_iter = iter->value->GetStart(); node_iter != nullptr; node_iter = node_iter->next)
        {
            std::cout << "[ pos: " << node_iter->value->column_id << " , " << std::dec << node_iter->value->value << "] ";
        }
        std::cout << '\n';
    }
    return 0;
}
