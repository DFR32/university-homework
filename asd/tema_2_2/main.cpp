#include "asd_linked_list.hpp"
#include "asd_linked_list.inl"

#include <cstdio>
#include <iostream>
#include <cstdint>
#include <iomanip>
#include <cstring>

template <typename T>
void SafeDeleteListRef(T& ptr)
{
    delete[] ptr;
    ptr = nullptr;
}

void count_evens_and_average_odds()
{
	// Variables we use to keep track of the data
	int64_t odd_sum = 0;
	size_t even_count = 0, odd_count = 0;

	// Create a new linked list
	LinkedList<int64_t> NewList;

	// Create a memory region to store the nodes
	LLNode<int64_t>* node_memory_area = new LLNode<int64_t>[10];

	// Set the value of all the nodes
	for(int64_t i = 0; i < 10; ++i)
	{
		node_memory_area[i].value = (41 * i) % 256;
		NewList.AddNodeToStart(&node_memory_area[i]);
	}

	for(LLNode<int64_t>* i = NewList.GetStart(); i != nullptr; i = i->next)
	{
		if(i->value & 1)
		{
			odd_sum += i->value;
			++ odd_count;
		}
		else
		{
			++ even_count;
		}
	}

	// Display the list
	NewList.Display(std::cout);

	std::cout << "There are " << even_count << " even numbers.\n";
	std::cout << "The mean average of the odd numbers is " << static_cast<double>(static_cast<double>(odd_sum) / static_cast<double>(odd_count)) << " .\n";

	// Clear the list (equivalent to start = nullptr)
	NewList.Clear();

	// Deallocate the memory
	SafeDeleteListRef(node_memory_area);
}

void add_mean_average()
{
	// Create a new linked list
	LinkedList<int64_t> NewList;

	// Create a memory region to store the nodes
	LLNode<int64_t>* node_memory_area = new LLNode<int64_t>[10];

	LLNode<int64_t>* last_node = nullptr, *tmp_node = nullptr;

	unsigned element_count = 0;
	int64_t mean_average = 0;

	// Set the value of all the nodes
	for(int64_t i = 0; i < 10; ++i)
	{
		node_memory_area[i].value = (41 * i) % 256;
		NewList.AddNodeToStart(&node_memory_area[i]);
	}

	for(LLNode<int64_t>* i = NewList.GetStart(); i != nullptr; i = i->next)
	{
		++ element_count;

		if(element_count == 2)
		{
			mean_average = (last_node->value + i->value) / 2;

			last_node = i;

			tmp_node = new LLNode<int64_t>();

			tmp_node->value = mean_average;
			tmp_node->next = i->next;
			i->next = tmp_node;
			i = tmp_node;

			element_count = 0;
		}
		else
			last_node = i;
	}

	// Display the list
	NewList.Display(std::cout);

	// Clear the list
	NewList.Clear();

	// Deallocate the memory
	SafeDeleteListRef(node_memory_area);
}

void big_number_sum()
{
	char first_number[33] = { '0' };
	char second_number[33] = { '0' };

	// Read the numbers
	scanf("%32s", first_number);
	scanf("%32s", second_number);

	// Create a new linked list
	LinkedList<uint8_t> ListA, ListB;

	// Create a memory region to store the nodes
	LLNode<uint8_t>* node_memory_area_A = new LLNode<uint8_t>[32];

	// Create a memory region to store the nodes
	LLNode<uint8_t>* node_memory_area_B = new LLNode<uint8_t>[32];

	for(int16_t i = 0; i < 32; ++i)
	{
		if(first_number[i] >= '0' && first_number[i] <= '9')
			node_memory_area_A[i].value = static_cast<uint8_t>(first_number[i] - '0');
		else node_memory_area_A[i].value = 0;

		if(second_number[i] >= '0' && second_number[i] <= '9')
			node_memory_area_B[i].value = static_cast<uint8_t>(second_number[i] - '0');
		else node_memory_area_B[i].value = 0;

		if(node_memory_area_A[i].value > 9 || node_memory_area_B[i].value > 9)
		{
			std::cerr << "Cannot assign " << (unsigned)node_memory_area_A[i].value << " to A.\nNor "<< (unsigned)node_memory_area_B[i].value << " to B.\n";
			std::cerr << "Digit i: " << (unsigned)first_number[i] << "\nDigit j: " << (unsigned)second_number[i] << '\n';
			return;
		}

		if(i < strlen(first_number))
		{
			ListA.AddNodeToStart(&node_memory_area_A[i]);
			ListB.AddNodeToStart(&node_memory_area_B[i]);
		}
		else
		{
			ListA.AddNodeToEnd(&node_memory_area_A[i]);
			ListB.AddNodeToEnd(&node_memory_area_B[i]);
		}
	}

	// Display the list as it is
	std::cout << std::setw(33) << "A\n";
	ListA.Display(std::cout);
	std::cout << "\n\n\n";

	std::cout << std::setw(33) << "B\n";
	ListB.Display(std::cout);
	std::cout << "\n\n\n";

	std::cout << std::setw(37) << "A + B\n";

	// Perform the operations
	for(LLNode<uint8_t>* i = ListA.GetStart(), * j = ListB.GetStart(); ((i != nullptr) && (j != nullptr));)
	{
		i->value = (i->value + j->value);

		if(i->value > 9)
		{
			++ i->next->value;
			i->value = i->value % 10;
		}

		i = i->next;
		j = j->next;
	}

	// Display the result linked list
	ListA.Display(std::cout);

	// Clear the lists
	ListA.Clear();
	ListB.Clear();

	// Deallocate the memory
	SafeDeleteListRef(node_memory_area_A);
	SafeDeleteListRef(node_memory_area_B);
}

void big_number_product()
{
	char first_number[33] = { '0' };
	unsigned second_number = 0;

	// Read the numbers
	scanf("%32s", first_number);
	scanf("%d", &second_number);

	// Create a new linked list
	LinkedList<uint8_t> ListA, List_tmp;

	// Create a memory region to store the nodes
	LLNode<uint8_t>* node_memory_area_A = new LLNode<uint8_t>[32];
	LLNode<uint8_t>* backup_memory		= new LLNode<uint8_t>[32];

	for(int16_t i = 0; i < 32; ++i)
	{
		if(first_number[i] >= '0' && first_number[i] <= '9')
			node_memory_area_A[i].value = static_cast<uint8_t>(first_number[i] - '0');
		else node_memory_area_A[i].value = 0;

		if(node_memory_area_A[i].value > 9)
			return;

		backup_memory[i].value = node_memory_area_A[i].value;

		if(i < strlen(first_number))
		{
			ListA.AddNodeToStart(&node_memory_area_A[i]);
			List_tmp.AddNodeToStart(&backup_memory[i]);
		}
		else
		{
			ListA.AddNodeToEnd(&node_memory_area_A[i]);
			List_tmp.AddNodeToEnd(&backup_memory[i]);
		}
	}

	// Display the list as it is
	std::cout << std::setw(33) << "A\n";
	ListA.Display(std::cout);
	std::cout << "\n\n\n";

	std::cout << std::setw(33) << "B\n";
	List_tmp.Display(std::cout);
	std::cout << "\n\n\n";

	std::cout << std::setw(37) << "A * B\n";


	// Perform the operations
	for(unsigned k = 0; k < second_number - 1; ++k)
	{
		for(LLNode<uint8_t>* i = ListA.GetStart(), * j = List_tmp.GetStart(); ((i != nullptr) && (j != nullptr));)
		{
			i->value = (i->value + j->value);

			if(i->value > 9)
			{
				++ i->next->value;
				i->value = i->value % 10;
			}

			i = i->next;
			j = j->next;
		}
	}

	// Display the result linked list
	ListA.Display(std::cout);

	// Clear the lists
	ListA.Clear();
	List_tmp.Clear();

	// Deallocate the memory
	SafeDeleteListRef(node_memory_area_A);
	SafeDeleteListRef(backup_memory);
}

int main()
{
	//count_evens_and_average_odds();
	//add_mean_average();
	big_number_sum();
	//big_number_product();
	return 0;
}
