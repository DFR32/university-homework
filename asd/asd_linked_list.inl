#include "asd_linked_list.hpp"

#include <iomanip>

template <typename T>
void LinkedList<T>::Display(std::ostream& outputStream)
{
    if(start == nullptr)
    {
        outputStream << "LinkedList::Display: No nodes to display.\n";
        return;
    }

    outputStream << std::setw(20) << "Node" << std::setw(15) << "Value" << std::setw(25) << "Next node" << '\n';

    for(LLNode<T>* i = start; i != nullptr; i = i->next)
        outputStream << std::setw(20) << std::hex << reinterpret_cast<uint64_t>(i) << std::setw(15) << std::dec << (unsigned)i->value << std::setw(25) << std::hex << reinterpret_cast<uint64_t>(i->next) << '\n';
}

template <typename T>
void LinkedList<T>::AddNodeToStart(LLNode<T>* new_start)
{
    new_start->next = start;
    start = new_start;
}

template <typename T>
void LinkedList<T>::AddNodeToEnd(LLNode<T>* new_end)
{
    new_end->next = nullptr;

    for(LLNode<T>* i = this->start; i != nullptr; i = i->next)
        if(i->next == nullptr)
        {
            i->next = new_end;
            return;
        }
}

template <typename T>
void LinkedList<T>::AddIntermediaryNode(LLNode<T>* new_node, size_t position)
{
    size_t node_count = 0;

    for(LLNode<T>* i = this->start; i != nullptr; i = i->next)
    {
        if(position == node_count)
        {
            new_node->next = new_node;
            std::swap(new_node->next, i->next);
            return;
        }

        ++ node_count;
    }
}

template <typename T>
LLNode<T>* LinkedList<T>::GetNodeAtPosition(size_t position)
{
    size_t node_count = 0;

    for(LLNode<T>* i = this->start; i != nullptr; i = i->next)
    {
        if(position == node_count)
        {
            return i;
        }

        ++ node_count;
    }

    return nullptr;
}

template <typename T>
LLNode<T>* LinkedList<T>::GetNodeWithValue(T val)
{
    for(LLNode<T>* i = this->start; i != nullptr; i = i->next)
    {
        if(i->value == val)
        {
            return i;
        }
    }

    return nullptr;
}

template <typename T>
LLNode<T>* LinkedList<T>::GetStart()
{
    return this->start;
}

template <typename T>
void LinkedList<T>::SetStart(LLNode<T>* st)
{
	this->start = st;
}

template <typename T>
LLNode<T>* LinkedList<T>::RemoveNodeAtPosition(size_t position)
{
    LLNode<T>* node_at_position = this->GetNodeAtPosition(position);
    LLNode<T>* temporary_node = nullptr;

    if(node_at_position == nullptr)
        return nullptr;

    if(node_at_position->next != nullptr)
    {
        node_at_position->value = node_at_position->next->value;

        temporary_node = node_at_position->next;
        node_at_position->next = node_at_position->next->next;

        return temporary_node;
    }
    else
    {
        for(LLNode<T>* i = start; i != nullptr; i = i->next)
        {
            if(i->next == node_at_position)
            {
                i->next = nullptr;

                return node_at_position;
            }
        }
    }

    return nullptr;
}

template <typename T>
LLNode<T>* LinkedList<T>::RemoveNodeWithValue(T val)
{
    LLNode<T>* node_at_position = this->GetNodeWithValue(val);

    LLNode<T>* temporary_node = nullptr;

    if(node_at_position == nullptr)
        return nullptr;

    if(node_at_position->next != nullptr)
    {
        node_at_position->value = node_at_position->next->value;

        temporary_node = node_at_position->next;
        node_at_position->next = node_at_position->next->next;

        return temporary_node;
    }
    else
    {
        for(LLNode<T>* i = start; i != nullptr; i = i->next)
        {
            if(i->next == node_at_position)
            {
                i->next = nullptr;

                return node_at_position;
            }
        }
    }

    return nullptr;
}

// Our design leaves room for user implementation
// We would rather return the start node for users to clear
// ... than do the cleanup ourselves (as we don't know...
// what method of memory allocation was used)
template <typename T>
void LinkedList<T>::Clear()
{
    start = nullptr;
}

template <typename T>
void LinkedList<T>::Apply(std::function<T(T&)> recursive_function)
{
    for(LLNode<T>* i = start; i != nullptr; i = i->next)
            i->value = recursive_function(i->value);
}
