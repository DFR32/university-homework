#include "asd_linked_list.hpp"
#include "asd_linked_list.inl"

typedef struct
{
	uint32_t idx;
	int32_t value;
} element;

void display_vector(LinkedList<element>* vector_t)
{
	printf("%-30s%-25s%-25s\n\n", "Address", "Index", "Value");
	for(LLNode<element>* i = vector_t->GetStart(); i != nullptr; i = i->next)
	{
		printf("%-30x%-25u%-25d\n", i, i->value.idx, i->value.value);
	}

	printf("\n\n");
}

void add_vectors(LinkedList<element>* p_1, LinkedList<element>* p_2)
{
	LLNode<element>* tmp_element = nullptr;

	for(LLNode<element>* j = p_2->GetStart(); j != nullptr; j = j->next)
	{
		tmp_element = nullptr;

		for(LLNode<element>* i = p_1->GetStart(); i != nullptr; i = i->next)
		{
			if(i->value.idx == j->value.idx)
			{
				tmp_element = i;
				break;
			}
		}

		if(tmp_element == nullptr)
		{
			tmp_element = new LLNode<element>();

			tmp_element->value.idx = j->value.idx;
			tmp_element->value.value = j->value.value;

			p_1->AddNodeToStart(tmp_element);
		}
		else
			tmp_element->value.value += j->value.value;
	}
}

int32_t scalar_product(LinkedList<element>* p_1, LinkedList<element>* p_2)
{
	int32_t result_product = 0;

	for(LLNode<element>* i = p_1->GetStart(); i != nullptr; i = i->next)
	{
		for(LLNode<element>* j = p_2->GetStart(); j != nullptr; j = j->next)
		{
			if(j->value.idx == i->value.idx)
			{
				result_product += (j->value.value * i->value.value);
				break;
			}
		}
	}

	return result_product;
}

int main()
{
	LinkedList<element>* vec_1  = new LinkedList<element>(nullptr);
	LinkedList<element>* vec_2  = new LinkedList<element>(nullptr);
	LLNode<element>* new_elem   = nullptr;

	element tmp_element = { 1, 17 };

	for(uint32_t i = 0; i < 7; ++i)
	{
		tmp_element.idx += 1;
		tmp_element.value += 2;

		new_elem = new LLNode<element>();
		new_elem->value = tmp_element;

		vec_1->AddNodeToStart(new_elem);
	}

	tmp_element.idx = 1;

	for(uint32_t i = 0; i < 5; ++i)
	{
		tmp_element.idx += 2;
		tmp_element.value += 5;

		new_elem = new LLNode<element>();
		new_elem->value = tmp_element;

		vec_2->AddNodeToStart(new_elem);
	}

	display_vector(vec_1);
	printf("\n\n\n");

	display_vector(vec_2);
	printf("\n\n\n");

	add_vectors(vec_1, vec_2);
	display_vector(vec_1);

	printf("\n\n\n");

	printf("Scalar product: %d\n", scalar_product(vec_1, vec_2));

	return 0;
}