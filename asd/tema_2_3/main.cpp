#include "asd_linked_list.hpp"
#include "asd_linked_list.inl"

#include <cstdio>
#include <iostream>
#include <cstdint>
#include <iomanip>
#include <cstring>
#include <cstdlib>
#include <cmath>

typedef struct 
{
	int32_t degree, coefficient;
} polynomial;

template <typename T>
void SafeDeleteListRef(T& ptr)
{
    delete[] ptr;
    ptr = nullptr;
}

LinkedList<polynomial>* construct_default_polynomial()
{
	// Seed the linear congruence solver (calling this function many times
	// ... in a short amount of time will lead to repetition!)
	//srand(time(0));

	// Create a temporary structure
	polynomial tmp_polynomial = { 1, 1 };

	// Create a new linked list
	LinkedList<polynomial>* NewList = new LinkedList<polynomial>();

	// Create a memory region to store the nodes
	LLNode<polynomial>* node_memory_area = new LLNode<polynomial>[10];

	// Set the value of all the nodes
	for(int64_t i = 0; i < 10; ++i)
	{
		// Reccurence formula
		tmp_polynomial.degree 		= tmp_polynomial.degree + (rand() % (i + 1));
		tmp_polynomial.coefficient  = (rand() % (i + 3)) + 1;


		node_memory_area[i].value = tmp_polynomial;
		NewList->AddNodeToStart(&node_memory_area[i]);
	}

	return NewList;
}

void display_polynomial(LinkedList<polynomial>* polynomial_t)
{
	printf("%-30s%-25s%-25s\n\n", "Address", "Degree", "Coefficient");
	for(LLNode<polynomial>* i = polynomial_t->GetStart(); i != nullptr; i = i->next)
	{
		printf("%-30x%-25u%-25u\n", i, i->value.degree, i->value.coefficient);
	}

	printf("\n\n");
}

template <typename T>
void multiply_by_scalar(LinkedList<polynomial>* polynomial_t, T scalar)
{
	for(LLNode<polynomial>* i = polynomial_t->GetStart(); i != nullptr; i = i->next)
		i->value.coefficient *= scalar;
}

int64_t evaluate_polynomial(LinkedList<polynomial>* polynomial_t, uint32_t point)
{
	int64_t sum = 0;

	for(LLNode<polynomial>* i = polynomial_t->GetStart(); i != nullptr; i = i->next)
			sum += pow(point, i->value.degree) * i->value.coefficient;

	return sum;
}

LinkedList<polynomial>* add_polynomials(LinkedList<polynomial>* p_1, LinkedList<polynomial>* p_2)
{
	LinkedList<polynomial>* result_polynomial = new LinkedList<polynomial>(nullptr);
	LLNode<polynomial>*		tmp_node 		  = nullptr;
	bool found_same_degree = true;

	printf("\n\n\n\n\n");

	display_polynomial(p_1);

	printf("\n\n\n\n\n");

	display_polynomial(p_2);

	printf("\n\n\n\n\n");

	for(LLNode<polynomial>* i = p_1->GetStart(); i != nullptr; i = i->next)
	{
		tmp_node = new LLNode<polynomial>();
		tmp_node->value = i->value;

		result_polynomial->AddNodeToStart(tmp_node);
	}

	for(LLNode<polynomial>* i = p_2->GetStart(); i != nullptr; i = i->next)
	{
		found_same_degree = false;

		for(LLNode<polynomial>* j = result_polynomial->GetStart(); j != nullptr; j = j->next)
		{
			if(i->value.degree == j->value.degree)
			{
				j->value.coefficient += i->value.coefficient;

				found_same_degree = true;
				break;
			}
		}

		if(!found_same_degree)
		{
			tmp_node = new LLNode<polynomial>();

			tmp_node->value.degree = i->value.degree;
			tmp_node->value.coefficient = i->value.coefficient;

			result_polynomial->AddNodeToStart(tmp_node);
		}
	}

	return result_polynomial;
}

LinkedList<polynomial>* reduce_double_polynomial(LinkedList<polynomial>* p)
{
	LinkedList<polynomial>* result_polynomial = new LinkedList<polynomial>(nullptr);

	LLNode<polynomial>* tmp_node = nullptr;

	bool found_double_occurence = false;

	polynomial tmp_values = { 0, 0 };

	for(LLNode<polynomial>* i = p->GetStart(); i != nullptr; i = i->next)
	{
		found_double_occurence = false;
		tmp_values.degree = i->value.degree;
		tmp_values.coefficient = i->value.coefficient;

		for(LLNode<polynomial>* double_ctr = result_polynomial->GetStart(); double_ctr != nullptr; double_ctr = double_ctr->next)
		{
			if(double_ctr->value.degree == i->value.degree)
			{
				found_double_occurence = true;
				break;
			}
		}

		if(found_double_occurence)
			continue;

		for(LLNode<polynomial>* j = p->GetStart(); j != nullptr; j = j->next)
		{
			if( i == j )
				continue;

			if(i->value.degree == j->value.degree)
			{
				tmp_values.degree = j->value.degree;
				tmp_values.coefficient += j->value.coefficient;
			}
		}

		tmp_node = new LLNode<polynomial>();

		tmp_node->value = tmp_values;
		result_polynomial->AddNodeToStart(tmp_node);
	}

	SafeDeleteListRef(p);
	return result_polynomial;
}

LinkedList<polynomial>* multipy_polynomials(LinkedList<polynomial>* p_1, LinkedList<polynomial>* p_2)
{
	LinkedList<polynomial>* result_polynomial = new LinkedList<polynomial>(nullptr);
	LLNode<polynomial>* tmp_node = nullptr;

	for(LLNode<polynomial>* i = p_1->GetStart(); i != nullptr; i = i->next)
	{
		for(LLNode<polynomial>* j = p_2->GetStart(); j != nullptr; j = j->next)
		{
			tmp_node = new LLNode<polynomial>();

			tmp_node->value.degree = i->value.degree + j->value.degree;
			tmp_node->value.coefficient = i->value.coefficient * j->value.coefficient;

			result_polynomial->AddNodeToStart(tmp_node);
		}
	}

	result_polynomial = reduce_double_polynomial(result_polynomial);
	return result_polynomial;
}

int main()
{
	LinkedList<polynomial>* default_polynomial = construct_default_polynomial();
	LinkedList<polynomial>* another_polynomial = construct_default_polynomial();

	display_polynomial(default_polynomial);

	multiply_by_scalar(default_polynomial, 20);

	display_polynomial(default_polynomial);

	int64_t sum = evaluate_polynomial(default_polynomial, 3);

	printf("Sum: %lld\n", sum);


	LinkedList<polynomial>* result_added_polynomial = add_polynomials(default_polynomial, another_polynomial);

	display_polynomial(result_added_polynomial);

	LinkedList<polynomial>* result_multiplied_polynomial = multipy_polynomials(default_polynomial, another_polynomial);

	display_polynomial(result_multiplied_polynomial);

	SafeDeleteListRef(default_polynomial);
	SafeDeleteListRef(another_polynomial);
	SafeDeleteListRef(result_added_polynomial);
	SafeDeleteListRef(result_multiplied_polynomial);
	return 0;
}
