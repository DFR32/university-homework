#include <cstdio>
#include <cstdint>
#include <stdexcept>

template <typename T>
T find_majoritary_element(T* vec, size_t size)
{
	T 		tmp_majority = 0;
	size_t  majority_ctr = 0;


	// Iterate the vector using a Boyer-Moore voting approach
	for(size_t i = 0; i < size; ++i)
	{

		// Check if the counter is 0, if it is
		// ... then set it to 1 and maek our guess
		if(majority_ctr == 0)
		{
			tmp_majority = vec[i];
			majority_ctr = 1;
		}

		// Ternary operator to decide whether to increment
		// ... the counter of occurences or not
		else (tmp_majority == vec[i]) ? ++majority_ctr : --majority_ctr;
	}

	// Set the majority counter to 0
	majority_ctr = 0;

	// Iterate the vector again to confirm the majoritary element
	for(size_t i = 0; i < size; ++i)
	{
		if(tmp_majority == vec[i])
			++ majority_ctr;
	}

	// Confirm our assumptions (This makes our algorithm O(2 * n))
	if(majority_ctr > (size / 2))
		return tmp_majority;
	else throw(std::runtime_error("Error: No majoritary element was found in this vector!"));

	// Something went bad if we got here
	return T();
}

int main()
{
	// Provide a test array to work with
	uint32_t test_array[] = { 4, 4, 2, 2, 6, 6 };

	// Get the majoritary element
	uint32_t majoritary_element = find_majoritary_element<uint32_t>(test_array, sizeof(test_array) / sizeof(uint32_t));

	// Display it
	printf("Majoritary element: %u\n", majoritary_element);
	return 0;
}