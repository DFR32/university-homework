#ifndef ASD_LINKED_LIST
#define ASD_LINKED_LIST

#include <iostream>
#include <functional>

template <typename T>
struct LLNode
{
    T value;
    LLNode* next;
};

template <typename T>
class LinkedList
{
    // Private variables
private:
    LLNode<T>* start;

public:
    // Constructors
    LinkedList() :
        start(nullptr)
    {
    }

    LinkedList(LLNode<T>* given_start) :
        start(given_start)
    {
    }

    // Methods for displaying the linked list
    void Display(std::ostream& outputStream);

    // Methods for adding new nodes
    void AddNodeToStart(LLNode<T>* new_start);
    void AddNodeToEnd(LLNode<T>* new_end);
    void AddIntermediaryNode(LLNode<T>* new_node, size_t position);

    // Data lookup methods
    LLNode<T>* GetNodeAtPosition(size_t position);
    LLNode<T>* GetNodeWithValue(T val);
    LLNode<T>* GetStart();
	
	void SetStart(LLNode<T>* st);

    // Data removal methods
    LLNode<T>* RemoveNodeAtPosition(size_t position);
    LLNode<T>* RemoveNodeWithValue(T val);
    void Clear();

    // Apply lambda method
    void Apply(std::function<T(T&)> recursive_function);
};


#endif // ASD_LINKED_LIST
